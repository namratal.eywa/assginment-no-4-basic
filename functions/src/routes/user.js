const firebase = require('firebase');
const jwt = require('jsonwebtoken');
require('firebase/firestore');
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const db = admin.firestore();
const fieldValue = admin.firestore.FieldValue;

function fetchuserlist() {
  return new Promise(function(resolve, reject){
    setTimeout(function(){
      const userRecord = db.collection('user');
      userRecord.get().then((querySnapshot) => {
        const tempDoc = []
        querySnapshot.forEach((doc) => {
           tempDoc.push({ id: doc.id, ...doc.data() })
        })
        const error = false;
        if(!error){
          return resolve(console.log(tempDoc));
        }
        else{
          reject("Data not fetched");
        }
     })
    }, 1000);
  })
}

//Get all user list
exports.getUserList = async(req, res) => {
  try {
    const list = await fetchuserlist();
    //return res.json({list});
    return res.status(200).send({message: "Data Fetched Successfully"});
  } catch (error) {
    res.status(500).send(error); 
  }
}
//Get user by Id 
exports.getUserById = (req, res) => {
  try {
    const userId = req.params.id;
    admin
    .auth()
    .getUser(userId)
    .then((userRecord) => {
      // See the UserRecord reference doc for the contents of userRecord.
      console.log(`Successfully fetched user data: ${userRecord.email}`);
      res.json({userRecord});
      //console.log("User Data: ", req.userData);
    })
    .catch((error) => {
      console.log('Error fetching user data:', error);
    });
  } catch (error) {
    res.status(500).send(error); 
  }
}
//Add new user
exports.addNewUser = (req, res) => {
  try {
    const {uid, firstname, lastname, email, password} = req.body;
    admin
    .auth()
    .createUser({
      uid: uid,
      firstname: firstname,
      lastname: lastname,
      email: email,
      emailVerified: false,
      password: password,
      disabled: false,
    })
    .then((userRecord) => {
      console.log('Successfully created new user:', userRecord.uid);
      const timestamp = fieldValue.serverTimestamp();
      const data = {
        key:`${userRecord.uid}`,
        firstname,
        lastname,
        email,
        createdAt: timestamp
      }
      const user = db.collection('user').doc(`${userRecord.uid}`).set(data, { merge: true});
    })
    .catch((error) => {
      console.log('Error creating new user:', error);
    });
    //console.log("user Id", uid);
    res.json({ message: "User Added Successfully"});
  } catch (error) {
    res.status(500).send(error);
  }
}
//login user
exports.login = (req, res) => {
  try {
    const login = req.body.login;
    const username = "namratalagshetti18@gmail.com";
    const password = "newpassword";
    if(username === login.username && password === login.password){
      const token = jwt.sign({userName: username, role: 'admin'}, "secret", {expiresIn: '24h'});
      return res.status(200).send({token: token});
    }
  } catch (error) {
    return res.status(401).send({error: "Invalid user"});
  }
}
//Update user by id
exports.updateUserById = (req, res) =>{
  try {
    const userId = req.params.id;
    const {firstname, lastname, email, password} = req.body;
    admin
    .auth()
    .updateUser(userId, {
      firstname: firstname,
      lastname: lastname,
      email: email,
      emailVerified: true,
      password: password,
      disabled: false,
    })
    .then((userRecord) => {
      console.log(`Successfully updated user:, ${userRecord.uid}`);
      const timestamp = fieldValue.serverTimestamp();
      const data = {
        firstname,
        lastname,
        email,
        updatedAt:timestamp
      }
      db.collection('user').doc(`${userRecord.uid}`).set(data, { merge: true});
      
      //db.collection('user').doc(`${userRecord.uid}`).update({ updatedAt: timestamp });
    })
    .catch((error) => {
      console.log('Error updating user:', error);
    });
    res.json({ message: "User updated Successfully"});
  } catch (error) {
    res.status(500).send(error);
  }
}
//Delete user by id
exports.deleteUserById = (req, res) => {
  try {
    const userId =req.params.id;
    admin
    .auth()
    .deleteUser(userId)
    .then(() => {
      console.log('Successfully deleted user');
      db.collection("user").doc(userId).delete();
    })
    .catch((error) => {
      console.log('Error deleting user:', error);
    });
    res.json({
      id:userId,
    })
  } catch (error) {
    res.status(500).send(error);
  }
}