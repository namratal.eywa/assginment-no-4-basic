const express = require("express");
const router = express.Router();
//const admin = require('firebase-admin');
const User = require("./user");
const auth_middelware = require("../middelware/auth_middelware");
//console.log("User Data: ", req.userData);
//router.use(authentication);
router.get('/:id', auth_middelware, User.getUserById);
router.get('/', auth_middelware, User.getUserList);
router.post('/', auth_middelware, User.addNewUser);
router.post('/login', User.login); 
router.put('/:id', auth_middelware, User.updateUserById);
router.delete("/:id", auth_middelware, User.deleteUserById);

module.exports = router;  

