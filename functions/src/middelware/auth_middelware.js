const jwt = require('jsonwebtoken');
module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    //console.log("Verify Token ", token);
    const decode = jwt.verify(token, 'secret');
    req.userData = decode;
    next();
  } catch (error) {
    res.status(401).json({
      error:"Invalid Token"
    });
  }
}